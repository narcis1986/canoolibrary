export interface IBook {
    id?: number;
    iSBN?: string;
    author?: string;
    title?: string;
    publisher?: string;
    copies?: number;
}

export class Book implements IBook {
    constructor(
        public id?: number,
        public iSBN?: string,
        public author?: string,
        public title?: string,
        public publisher?: string,
        public copies?: number
    ) {}
}
