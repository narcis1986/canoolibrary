package com.canoo.library.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Book entity. This class is used in BookResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /books?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BookCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter iSBN;

    private StringFilter author;

    private StringFilter title;

    private StringFilter publisher;

    private IntegerFilter copies;

    public BookCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getiSBN() {
        return iSBN;
    }

    public void setiSBN(StringFilter iSBN) {
        this.iSBN = iSBN;
    }

    public StringFilter getAuthor() {
        return author;
    }

    public void setAuthor(StringFilter author) {
        this.author = author;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getPublisher() {
        return publisher;
    }

    public void setPublisher(StringFilter publisher) {
        this.publisher = publisher;
    }

    public IntegerFilter getCopies() {
        return copies;
    }

    public void setCopies(IntegerFilter copies) {
        this.copies = copies;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BookCriteria that = (BookCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(iSBN, that.iSBN) &&
            Objects.equals(author, that.author) &&
            Objects.equals(title, that.title) &&
            Objects.equals(publisher, that.publisher) &&
            Objects.equals(copies, that.copies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        iSBN,
        author,
        title,
        publisher,
        copies
        );
    }

    @Override
    public String toString() {
        return "BookCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (iSBN != null ? "iSBN=" + iSBN + ", " : "") +
                (author != null ? "author=" + author + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (publisher != null ? "publisher=" + publisher + ", " : "") +
                (copies != null ? "copies=" + copies + ", " : "") +
            "}";
    }

}
