/**
 * View Models used by Spring MVC REST controllers.
 */
package com.canoo.library.web.rest.vm;
