# Canoo Library
This application was generated using JHipster 5.3.4, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.3.4](https://www.jhipster.tech/documentation-archive/v5.3.4). Because of this it has more functionalities then initial requested 

The application has a frontend written in Angular 6 and a backend where Spring Boot was used.

## Running the application
Install NodeJS on your host
Start the application by running
./gradlew

After the application has started type in a browser http://localhost:8080
Log in using admin/admin or user/user 
Next click on Entities and Book



